(function($) {
  Drupal.behaviors.thisworkedwell = {
    attach: function(context, settings) {
      $('.symp-menu').slicknav();

      $(".symp-menu li.sub-menu").hover(function() {
        $(this).addClass("open").children('a').addClass("hover");
      }, function() {
        $(this).removeClass("open").children('a').removeClass("hover");
      });
    }
  }
})(jQuery);