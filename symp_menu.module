<?php

/**
 * Implements hook_block_info().
 */
function symp_menu_block_info() {
  $blocks = array();
  $blocks['symp_menu']['info'] = t('Symposium Menu');
  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function symp_menu_block_configure($delta = '') {
  $form = array();
  if ($delta == 'symp_menu') {
    $vocabularies = taxonomy_get_vocabularies();
    $options = array('' => 'Select');
    foreach ($vocabularies as $vocabulary) {
      $options[$vocabulary->vid] = $vocabulary->name;
    }

    $form['symp_menu_vocab'] = array(
      '#type' => 'select',
      '#title' => t('Vocabulary'),
      '#default_value' => variable_get('symp_menu_vocab', array()),
      '#options' => $options,
    );
    $form['symp_menu_depth'] = array(
      '#type' => 'select',
      '#title' => t('Depth'),
      '#options' => array(
        '' => t('Show all'),
        'none' => t('No drop downs'),
        '1' => t('Single level drop down'),
      ),
    );
  }
  return $form;
}

/**
 * Implements hook_block_save().
 */
function symp_menu_block_save($delta = '', $edit = array()) {
  if ($delta == 'symp_menu') {
    variable_set('symp_menu_vocab', $edit['symp_menu_vocab']);
    variable_set('symp_menu_depth', $edit['symp_menu_depth']);
  }
}

/**
 * Implements hook_block_view().
 */
function symp_menu_block_view($delta = '') {
  $block = array();
  if ($delta == 'symp_menu') {
    $vocab = variable_get('symp_menu_vocab', array());
    if (!$vocab) return;

    drupal_add_css(drupal_get_path('module', 'symp_menu') .'/css/slicknav.css');
    drupal_add_css(drupal_get_path('module', 'symp_menu') .'/css/symp_menu.css');

    drupal_add_js(drupal_get_path('module', 'symp_menu') .'/js/jquery.slicknav.min.js');
    drupal_add_js(drupal_get_path('module', 'symp_menu') .'/js/symp_menu.js');

    $menu = array();
    $terms = taxonomy_get_tree($vocab, 0, 1);
    $level = 0;
    foreach ($terms as $term) {
      $menu[] = symp_menu_get_level($term);
    }
    $block['content'] = '<ul class="symp-menu">'. implode("\n", $menu) .'</ul>';
  }
  return $block;
}

function symp_menu_get_level($term, $level = 0) {
  $caret = ' <i class="fa fa-caret-down"></i>';
  if ($level > 0) $caret = ' <i class="fa fa-caret-right"></i>';
  $level++;
  $depth = variable_get('symp_menu_depth', '');
  $dropdown = '';
  $attributes = array();
  $output = '<li';
  if ($depth != 'none') {
    $children = taxonomy_get_children($term->tid, $term->vid);
    if (!empty($children)) {
      $attributes['html'] = TRUE;
      $dropdown .= '<ul>';
      foreach ($children as $child) {
        $dropdown .= symp_menu_get_level($child, $level);
      }
      $dropdown .= '</ul>';
      $output .= ' class="sub-menu"';
    }
  }
  $output .= '>';

  $output .= l(t($term->name) . ($dropdown != '' ? $caret : ''), 'taxonomy/term/'. $term->tid, $attributes);

  $output .= $dropdown .'</li>';

  return $output;
}